﻿using Microsoft.EntityFrameworkCore;
using todos_back_end.Configuration;
using todos_back_end.Models;
using todos_back_end.Seeders;

namespace todos_back_end.data
{
    public class TodosDbContext : DbContext
    {
        public TodosDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TodoCofiguration());
            modelBuilder.Seed();
        }
        public DbSet<Todo> Todos { get; set; }
    }
}
