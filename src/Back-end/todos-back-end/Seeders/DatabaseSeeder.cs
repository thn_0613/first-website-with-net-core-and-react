﻿using Microsoft.EntityFrameworkCore;
using todos_back_end.Models;

namespace todos_back_end.Seeders
{
    public static class DatabaseSeeder
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Todo>().HasData(
                new Todo()
                {
                    Id = 1,
                    Name = "Nhiệm vụ 1",
                    IsCompleted = false
                } ,
                new Todo()
                {
                    Id = 2,
                    Name = "Nhiệm vụ 2",
                    IsCompleted = false
                }
            );
        }
    }
}
