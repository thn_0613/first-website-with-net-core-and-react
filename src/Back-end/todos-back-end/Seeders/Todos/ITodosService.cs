﻿using todos_back_end.Models;

namespace todos_back_end.Seeders.Todos
{
    public interface ITodosService
    {
        List<Todo> GetTodos();
        Boolean AddTodo(Todo todo);
        Boolean UpdateTodo(Todo todo);
        Boolean DelTodo(int id);
    }
}
